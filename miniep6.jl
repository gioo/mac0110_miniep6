# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    primeiro_impar=1
    ultimo_impar=primeiro_impar+2*(n-1)
    while n*(primeiro_impar+ultimo_impar)/2<n^3
        primeiro_impar+=2
        ultimo_impar=primeiro_impar+2*(n-1)
    end
    return primeiro_impar
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    primeiro_impar=1
    ultimo_impar=primeiro_impar+2*(m-1)
    while m*(primeiro_impar+ultimo_impar)/2<m^3
        primeiro_impar+=2
        ultimo_impar=primeiro_impar+2*(m-1)
    end
    termo=primeiro_impar
    print(m," ",m^3," ")
    while termo<=ultimo_impar
        print(termo," ")
        termo+=2
    end
end

function mostra_n(n)
    linha=1
    while linha<=n
        imprime_impares_consecutivos(linha)
        println("")
        linha+=1
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
# test()
